NAME
  tlsdb-python2 \- manage SRP verifier databases

USAGE EXAMPLE
  $ tlsdb-python2 createsrp myverifiers

  $ tlsdb-python2 add myverifiers alice abra123cadabra 1024

  $ tls-python2 server -v myverifiers localhost:4443

  $ tls-python2 client localhost:4443 alice abra123cadabra
  
DESCRIPTION
  tlsdb-python2 lets you manage SRP verifier databases. These databases are used by
  a TLS server when authenticating clients with SRP.

OPTIONS
  For a list of options, please see the output of $ tlsdb-python2.

SEE ALSO
  tls-python2(1)

AUTHORS
  tlslite-ng is developed by Hubert Kario <hkario@redhat.com>
